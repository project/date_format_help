Original documentation by the PHP Group, reproduced under the Creative Commons
Attribution 3.0 license.

Original Documentation at: http://us3.php.net/manual/en/function.date.php
PHP License Page: http://us3.php.net/license/
